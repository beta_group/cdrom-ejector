# CDROM Eject
This is a small program that can eject CDROMs from the same network if they have
a PHP server and the small (a few MBs) client.

This program is used to impress new people that are interested in computers and
to show them what they can do with a few lines of code.

## Setup guide
This program have only 2 requirments:
1. PHP server on the "infected" computers
2. Local server for the eject panel (can be used manually without the panel)