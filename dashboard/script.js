'use strict';
function ejectPc(ip) {
	let request = new XMLHttpRequest();
	request.open('GET', `http://${ip}`, true);
	request.send();
}

function addPc(pc) {
	let list = document.getElementById('pc-list');
	let item = document.createElement('li');
	item.classList.add('pc');
	
	let name = document.createElement('span');
	name.innerHTML = pc.ip;
	item.appendChild(name);

	let seperator = document.createElement('div');
	seperator.classList.add('flex-seperator');
	item.appendChild(seperator);

	let button = document.createElement('span');
	button.classList.add('eject-button');
	button.innerHTML = 'Eject';
	button.addEventListener('click', function() {
		ejectPc(pc.ip);
	}, false);
	item.appendChild(button);

	list.appendChild(item);
}

function ping(pc) {
	let request = new XMLHttpRequest();
	request.open('GET', `http://${pc.ip}`, true);

	request.onload = function() {
		console.log('test');
	};// addPc.bind(this, pc);
	request.onerror = function() {
		console.log(`http://${pc.ip} err`);
	};

	request.send();
}

function onLoad() {
	if (this.status >= 200 && this.status < 400) {
		// Success!
		let data = JSON.parse(this.response);
		for(let pc of data) {
			ping(pc);
		}
	} else {
		// We reached our target server, but it returned an error
		alert(`ERR ${this.status}`);
	}
}

function onError() {
	// There was a connection error of some sort
	console.log('Connection error');
}

let request = new XMLHttpRequest();
request.open('GET', 'http://invisibleun1corn.pe.hu/search.php', true);

request.onload = onLoad;
request.onerror = onError;

request.send();

